from distutils.core import setup
import py2exe
import numpy

setup(console=['test.py'],
 options={
               'py2exe': {
                          'packages' :  ['numpy']
                          }
                }
)
