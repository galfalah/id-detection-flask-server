from flask import Flask, render_template, request, redirect, url_for, send_from_directory, jsonify,session
from werkzeug import secure_filename
import json
import math
import time
import cv2
import imutils
import tensorflow as tf, sys
import os
import os.path
import logging
from datetime import datetime
from flask_failsafe import failsafe
from logging.handlers import TimedRotatingFileHandler

print("importing seesion")
import model

print("getting seesion")
sess = model.Session()

# Initialize the Flask application
app = Flask(__name__)
logging.basicConfig(filename='Logs/log.log', level=logging.DEBUG, format='%(asctime)s %(message)s')

logger = logging.getLogger("Rotating Log")
logger.setLevel(logging.INFO)
handler = TimedRotatingFileHandler("logRot.log", when="d", interval=1, backupCount=5)
logger.addHandler(handler)


# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config["MAX_DETECTIONS"] = 5
app.config["TREASHOLD"] = 0.85
app.config["IS_DEBUG"] = 'True'
app.config["SESSION_ID"] = '999'
app.config["IMAGE_HEIGHT"] = 600
app.config["IMAGE_WIDTH"] = 330
app.config["WINDOW_HEIGHT"] = 120
app.config["WINDOW_WIDTH"] = 220
app.config["VERTICAL_STEP"] = 100
app.config["HORIZONTAL_STEP"] = 55
app.config["HORIZONTAL_SLICE_COUNT"] = 2
app.config["VETICAL_SLICE_COUNT"] = 2
app.config["ERROR_MESSAGE"] = "No Error"
app.config["STATUS"] = 1

# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS']=set(['jpg', 'jpeg'])


# For a given file, return whether it's an allowed type or not
"""def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']
"""
# This route will show a form to perform an AJAX request
# jQuery is loaded to execute the request and update the
# value of the operation
@failsafe
@app.route('/')
def index():
    """Renders File upload form as a default route"""
    return render_template('index.html')
@failsafe
@app.route('/deleteImages', methods=['POST'])
def delete_images():
    """Deletes images in origins and results folders """
    delete_files("origins")
    delete_files("results")
    return render_template('index.html')

@failsafe
@app.route('/validateAll', methods=['POST'])
def validate_all():
    """Validates all test images"""
    returnJSON = ""

    folder = "test_images"
    app.config["TREASHOLD"] = float(request.form['Treashold'])
    app.config["IS_DEBUG"] = request.form['Is_debug']
    app.config["SESSION_ID"] = str(request.form['Session_id'])
    app.config["IMAGE_HEIGHT"] = int(request.form['Image_height'])
    app.config["IMAGE_WIDTH"] = int(request.form['Image_width'])


    #with tf.Session() as sess:

    #tf.import_graph_def(app.grafh_def, name='')
    logger.info("graph file imported")
    logger.debug("graph file imported")
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

    logger.debug("tensor final_result")
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        if os.path.isfile(file_path):
            start_time = datetime.now().microsecond
            logger.debug("starting Session for %s", the_file)
            print("**")
            print(detect(file_path, start_time , False))
            returnJSON += json.dumps(detect(file_path, start_time , False , request)) + ", "
            logger.debug("[%s]",returnJSON[:-1])
    return jsonify("[%s]" % returnJSON[:-1])




# Route that will process the file upload
@failsafe
@app.route('/validate', methods=['POST'])
def validate_ids():
    """Validates Israeli ID Document"""
    start_time = datetime.now().microsecond
    logger.info('app started, start time %s', start_time)
    # Get the name of the uploaded file

    file = request.files['File']

    app.config["TREASHOLD"] = float(request.form['Treashold'])
    app.config["IS_DEBUG"] = request.form['Is_debug']
    app.config["SESSION_ID"] = str(request.form['Session_id'])
    app.config["IMAGE_HEIGHT"] = int(request.form['Image_height'])
    app.config["IMAGE_WIDTH"] = int(request.form['Image_width'])


    logger.info('got file: %s', file.filename)
    logger.info('config["TREASHOLD"]: %s', request.form['Treashold'])
    logger.info('config["IS_DEBUG"]: %s', request.form['Is_debug'])
    logger.info('config["SESSION_ID"]: %s', str(request.form['Session_id']))
    logger.info('config["IMAGE_HEIGHT"]: %s', str(request.form['Image_height']))
    logger.info('config["IMAGE_WIDTH"]: %s', str(request.form['Image_width']))



    file_name = secure_filename(file.filename)
        # Move the file form the temporal folder to
        # the upload folder we setup
    saved_file_name = os.path.join(app.config['UPLOAD_FOLDER'], file_name)
    file.save(saved_file_name)
    logger.info('file savedfile: %s', saved_file_name)
    return detect(file_name, start_time , True , request)

def delete_files(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        if os.path.isfile(file_path):
            os.unlink(file_path)
    return

def detect(file_name, start_time , is_singal_image , request):
    """Running detect init """
    if is_singal_image:
        file_name = os.path.join(app.config['UPLOAD_FOLDER'], file_name)
    logger.debug("graph file imported")
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
    logger.debug("starging Session")
    #return jsonify(scan_image(file_name, start_time, softmax_tensor, False))
    flag_find_id = False
    image = cv2.imread(file_name)
    logger.debug("file was read")
    height, width = image.shape[:2]
    res = image
    (window_width, window_height) = (app.config["WINDOW_WIDTH"] , app.config["WINDOW_HEIGHT"]) #220X120
    if height > width:
        scale = 240/width
        dim = (240,int(height * scale))
        logger.info('portrait')
    else:
        logger.info('landscape')
        scale = 426/width
        dim = (426,int(height * scale))

    logger.debug('dim:%s', dim)
    res = cv2.resize(image, dim,interpolation = cv2.INTER_AREA)
    logger.info('original image resized to %sX%s', image.shape[0], image.shape[1])

    label_lines = [line.rstrip() for line in tf.gfile.GFile('retrained_labels8000.txt')]
    index_of_label = 0
    ido_index = -1
    idn_index = -1
    license_index = -1
    negativ_index = -1
    #file_labels = open('retrained_labels8000.txt', 'r')
    for line in open('retrained_labels8000.txt', 'r'):
        print(line)
        if "ido" in line:
            ido_index = index_of_label
            print(ido_index)
            index_of_label += 1
        if "idn" in line:
            idn_index = index_of_label
            print(idn_index)
            index_of_label += 1
        if "negative" in line:
            negativ_index = index_of_label
            print(negativ_index)
            index_of_label += 1
        if "license" in line:
            license_index = index_of_label
            print(license_index)
            index_of_label += 1
    print(ido_index , idn_index , license_index , negativ_index)

    max_new_id_score = 0
    max_license_score = 0
    max_old_id_score = 0
    max_negative_score = 0
    new_id_count = 0
    old_id_count = 0
    license_count = 0
    total_count = 0
    app.config["STATUS"] = 1

    image_data = tf.gfile.FastGFile(file_name, 'rb').read()
    logger.warning('prediction started')
    predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0' : image_data})
    logger.warning('prediction ended')
    top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
    first_label = label_lines[top_k[0]]
    if first_label == "idn" and predictions[0][2] > app.config["TREASHOLD"] :
        new_id_count += 1
        app.config["STATUS"] = 0
    if first_label == "ido" and predictions[0][0] > app.config["TREASHOLD"]:
        old_id_count += 1
        app.config["STATUS"] = 0
    if first_label == "license" and predictions[0][1] > app.config["TREASHOLD"]:
        license_count += 1
        app.config["STATUS"] = 1
    if first_label == "negative" and predictions[0][3] > app.config["TREASHOLD"]:
        license_count += 1
        app.config["STATUS"] = 1
    #score_string = "%s,%s" %(y, x)
    for node_id in top_k:
        human_string = label_lines[node_id]
        score = predictions[0][node_id]
        logger.warning('%s (score = %.5f)', human_string, score)

        if human_string == "idn":
            max_new_id_score = max(max_new_id_score, score)
        if human_string == "license":
            max_license_score = max(max_license_score, score)
        if human_string == "ido":
            max_old_id_score = max(max_old_id_score, score)
        if human_string == "negative":
            max_negative_score = max(max_negative_score, score)

    end_time = datetime.now().microsecond
    print(start_time)
    print(end_time)
    time_taken = (end_time - start_time)
    print(time_taken)
    time_taken = time_taken/1000000
    print(time_taken)
    logger.info('app ended, end time %s', end_time)

    error_message = app.config["ERROR_MESSAGE"]
    status = app.config["STATUS"]
    treashold_actual = str(app.config["TREASHOLD"])
    session_id_actual = str(app.config["SESSION_ID"])
    is_debug_actual = app.config["IS_DEBUG"]
    image_height_actual = str(app.config["IMAGE_HEIGHT"])
    image_width_actual = str(app.config["IMAGE_WIDTH"])

    treashold_reqeust = str(request.form['Treashold'])
    session_id_reqeust = str(request.form['Session_id'])
    is_debug_reqeust = request.form['Is_debug']
    image_height_reqeust = str(request.form['Image_height'])
    image_width_reqeust = str(request.form['Image_width'])

    max_old_id_score =  str(round(max_old_id_score , 4))
    max_new_id_score = str(round(max_new_id_score , 4))
    max_license_score = str(round(max_license_score , 4))
    max_negative_score = str(round(max_negative_score , 4))



    actual = {"actual":{"TREASHOLD": treashold_actual ,"SESSION_ID": session_id_actual, "IS_DEBUG":is_debug_actual,"IMAGE_HEIGHT": image_height_actual , "IMAGE_WIDTH": image_width_actual}}

    request ={ "request":{"TREASHOLD": treashold_reqeust,"SESSION_ID": session_id_reqeust, "IS_DEBUG":is_debug_reqeust,"IMAGE_HEIGHT": image_height_reqeust , "IMAGE_WIDTH":image_width_reqeust}}

    return_data = {"file_name":file_name , "ido": max_old_id_score , "idn": max_new_id_score , "licence": max_license_score, "negative":max_negative_score, "error_message":error_message , "time_taken": time_taken,"status":status ,"app_params":[actual,request] }


    if is_singal_image:
        logger.debug("[%s]",return_data)
        return jsonify(return_data)
    else:
        logger.debug("[%s]",return_data)
        return return_data
if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("5000"),
        debug=True,
        processes=15
    )
