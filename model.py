import numpy as np
import tensorflow as tf


class Session(object):
    __instance = None

    def __new__(cls):
        if Session.__instance is None:
            print('creating singleton')

            graph = tf.Graph()
            graph_def = tf.GraphDef()
            with graph.as_default():
                with open('retraind_graph8000.pb', 'rb') as f:
                    graph_def.ParseFromString(f.read())
                _ = tf.import_graph_def(graph_def, name='')
            sess = tf.Session(graph=graph)
            print('Session created')

            # Warm up Session
            print('Warming up Session')
            image_data = tf.gfile.FastGFile('static/pic.jpg', 'rb').read()
            softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
            for i in range(10):
                _ = sess.run(softmax_tensor, {'DecodeJpeg/contents:0' : image_data})

            print('Session ready to serve')
            Session.__instance = sess
        return Session.__instance
