from flask import Flask, render_template, request, redirect, url_for, send_from_directory,jsonify
from werkzeug import secure_filename
import time
import cv2
import imutils
import tensorflow as tf, sys
import os
import os.path

"""data = {"success": False,"error": "detact more than one Id in image"}
return jsonify(data)"""


# Initialize the Flask application
app = Flask(__name__)

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# For a given file, return whether it's an allowed type or not
"""def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']
"""
# This route will show a form to perform an AJAX request
# jQuery is loaded to execute the request and update the
# value of the operation
@app.route('/')
def index():
    return render_template('index.html')


# Route that will process the file upload
@app.route('/upload', methods=['POST'])
def upload():
    # Get the name of the uploaded file
    file = request.files['file']
    # Check if the file is one of the allowed types/extensions
    #if file and allowed_file(file.filename):
        # Make the filename safe, remove unsupported chars
    filename = secure_filename(file.filename)
        # Move the file form the temporal folder to
        # the upload folder we setup
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return detect(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # Redirect the user to the uploaded_file route, which

        # will basicaly show on the browser the uploaded file
    #return redirect(url_for('upload_file',
    #                          filename=filename))


def pyramid(image, scale=1.5, minSize=(30, 30)):
	# yield the original image
	yield image

	# keep looping over the pyramid
	while True:
		# compute the new dimensions of the image and resize it
		w = int(image.shape[1] / scale)
		image = imutils.resize(image, width=w)

		# if the resized image does not meet the supplied minimum
		# size, then stop constructing the pyramid
		if image.shape[0] < minSize[1] or image.shape[1] < minSize[0]:
			break

		# yield the next image in the pyramid
		yield image


def sliding_window(image, stepSize, windowSize):
	#choose from ehere to start scan_image()
	height, width = image.shape[:2]
	start_height = height/3
	# slide a window across the image
	for y in range(0, image.shape[0], stepSize):
		for x in range(0, image.shape[1], stepSize):
			# yield the current window
			yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])

def scan_image(original_image_path):
	with tf.gfile.FastGFile('retraind_graph.pb') as f:
		grafh_def = tf.GraphDef()
		grafh_def.ParseFromString(f.read())
		_ = tf.import_graph_def(grafh_def, name='')
	print("1")
	softmax_tensor = tf.Session().graph.get_tensor_by_name('final_result:0')
	flag_find_id = False
	# load the image and define the window width and height
	image = cv2.imread(original_image_path)
	height, width = image.shape[:2]

	#resize image in case the image is big
	res = image
	(winW, winH) = (220,120)

	if height > width:
		print("portrait")
		#portrait
		res = cv2.resize(image,(330,600), interpolation = cv2.INTER_CUBIC)
		(winW, winH) = (220,120)
	else:
		print("landscape")
		#landscape
		res = cv2.resize(image,(600,330), interpolation = cv2.INTER_CUBIC)
		(winW, winH) = (330,220)



	# loop over the image pyramid
	for resized in pyramid(res, scale=1.5):
		if flag_find_id == True:
			break
		# loop over the sliding window for each layer of the pyramid
		for (x, y, window) in sliding_window(resized, stepSize=52, windowSize=(winW, winH)):
			if flag_find_id == True:
				break
			# if the window does not meet our desired window size, ignore it
			if window.shape[0] != winH or window.shape[1] != winW:
				continue

			# THIS IS WHERE YOU WOULD PROCESS YOUR WINDOW, SUCH AS APPLYING A
			# MACHINE LEARNING CLASSIFIER TO CLASSIFY THE CONTENTS OF THE
			# WINDOW
			#image_data = tf.gfile.FastGFile(image_path, 'rb').read()
			images_placeholder = tf.placeholder(tf.int32)

			# crop the sub image we scan and save as temp_crop image
			#crop_img = resized[y : y + winH , x : x + winW](this line can delete we used window insted croping )
			print("start save")
			cv2.imwrite("temp_crop.jpeg",window)
			print("end save")
			#image_data = tf.gfile.FastGFile("temp_crop.jpeg", 'rb').read()
			label_lines = [line.rstrip() for line in tf.gfile.GFile('retrained_labels.txt')]
			print("2")




			#original_image = tf.gfile.FastGFile(filename , 'rb').read() ##args["image"]
			image_data = tf.gfile.FastGFile("temp_crop.jpeg", 'rb').read()
			#predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0' : original_image})
			print("prediction start")
			predictions = tf.Session().run(softmax_tensor, {'DecodeJpeg/contents:0' : image_data})
			print("read image")
			top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
			first_label = label_lines[top_k[0]]
			print("prediction end")


			#just a test to chack witch sub image we are testing at this step
			"""cv2.imshow("Window_2", crop_img)
			cv2.waitKey(1000)
			cv2.destroyWindow("Window_2")"""

			top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]

			first_label = label_lines[top_k[0]]                    #0.9 the minimum presange of detaction
			if ( ( first_label == "idn" or first_label == "ido") and ( predictions[0][top_k[0]] > 0.9 ) ):
				flag_find_id = True
				# return true we detact ID
				data = {"isID": True ,"presange": float(predictions[0][top_k[0]]) , "type": first_label }
				return jsonify(data)
			#print results for each scan
			for node_id in top_k:
			    human_string = label_lines[node_id]
			    score = predictions[0][node_id]
			    print('%s (score = %.5f)' % (human_string, score))
			print('#####################################################')

			# draw the window just for test version
			#clone = resized.copy()
			#cv2.rectangle(clone, (x, y), (x + winW, y + winH), (0, 255, 0), 2)
			#cv2.imshow("Window", clone)
			#cv2.waitKey(1)
			# Id not found
	IdNotFounddata = {"isID": False ,"presange": 0 , "type": "not id" }
	return jsonify(IdNotFounddata)

def detect(filename):
	label_lines = [line.rstrip() for line in tf.gfile.GFile('retrained_labels.txt')]
	with tf.gfile.FastGFile('retraind_graph.pb') as f:
	    grafh_def = tf.GraphDef()
	    grafh_def.ParseFromString(f.read())
	    _ = tf.import_graph_def(grafh_def, name='')

	with tf.Session() as sess:
		softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
		original_image = tf.gfile.FastGFile(filename , 'rb').read() ##args["image"]
		predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0' : original_image})
		top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
		first_label = label_lines[top_k[0]]
		if ( ( first_label == "idn" or first_label == "ido") and ( predictions[0][top_k[0]] > 0.6 ) ):
			print("*ID on first try*")
			#return true we found ID
			data = {"isID": True ,"presange": float(predictions[0][top_k[0]]) , "type": first_label }
			return jsonify(data)
		else:
			print("*No ID start to ascan*")
			#start to scan image for ID
		print("close sses")
		sess.close()
	return scan_image(filename)


if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True
    )
