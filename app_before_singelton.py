

from flask import Flask, render_template, request, redirect, url_for, send_from_directory, jsonify,session
from werkzeug import secure_filename
import json
import math
import time
import cv2
import imutils
import tensorflow as tf, sys
import os
import os.path
import logging
from datetime import datetime
from flask_failsafe import failsafe




# Initialize the Flask application
app = Flask(__name__)
logging.basicConfig(filename='log.log', level=logging.DEBUG, format='%(asctime)s %(message)s')




# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config["MAX_DETECTIONS"] = 5
app.config["TREASHOLD"] = 0.85
app.config["IS_DEBUG"] = 'True'
app.config["SESSION_ID"] = '999'
app.config["IMAGE_HEIGHT"] = 600
app.config["IMAGE_WIDTH"] = 330
app.config["WINDOW_HEIGHT"] = 120
app.config["WINDOW_WIDTH"] = 220
app.config["VERTICAL_STEP"] = 100
app.config["HORIZONTAL_STEP"] = 55
app.config["HORIZONTAL_SLICE_COUNT"] = 2
app.config["VETICAL_SLICE_COUNT"] = 2


# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set([ 'jpg', 'jpeg'])

app.retrain_file = tf.gfile.FastGFile('retraind_graph.pb')
app.grafh_def = tf.GraphDef()
logging.debug("graph def loaded")
app.grafh_def.ParseFromString(app.retrain_file.read())
logging.debug("graph file loaded")

# For a given file, return whether it's an allowed type or not
"""def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']
"""
# This route will show a form to perform an AJAX request
# jQuery is loaded to execute the request and update the
# value of the operation
@failsafe
@app.route('/')
def index():
    """Renders File upload form as a default route"""
    return render_template('index.html')
@failsafe
@app.route('/deleteImages', methods=['POST'])
def delete_images():
	"""Deletes images in origins and results folders """
	delete_files("origins")
	delete_files("results")
	return render_template('index.html')

@failsafe
@app.route('/validateAll', methods=['POST'])
def validate_all():
	"""Validates all test images"""
	returnJSON = ""

	folder = "test_images"

	with tf.Session() as sess:

		tf.import_graph_def(app.grafh_def, name='')
		logging.debug("graph file imported")
		softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
		logging.debug("tensor final_result")
		for the_file in os.listdir(folder):
			file_path = os.path.join(folder, the_file)
			if os.path.isfile(file_path):
				start_time = datetime.now()
				logging.debug("starting Session for %s", the_file)
				returnJSON += json.dumps(scan_image(file_path, start_time, sess, softmax_tensor, True), ensure_ascii=False) + ", "
				logging.debug("[%s]",returnJSON[:-1])
	return jsonify("[%s]" % returnJSON[:-1])



# Route that will process the file upload
@failsafe
@app.route('/validate', methods=['POST'])
def validate_ids():
    """Validates Israeli ID Document"""
    start_time = datetime.now()
    logging.info('app started, start time %s', start_time)
    # Get the name of the uploaded file
    file = request.files['file']
    app.config["MAX_DETECTIONS"] = int(request.form['max_count'])
    app.config["TREASHOLD"] = float(request.form['treashold'])
    app.config["IS_DEBUG"] = request.form['is_debug']
    app.config["SESSION_ID"] = str(request.form['session_id'])
    app.config["IMAGE_HEIGHT"] = int(request.form['image_height'])
    app.config["IMAGE_WIDTH"] = int(request.form['image_width'])
    app.config["WINDOW_HEIGHT"] =int(request.form['window_height'])
    app.config["WINDOW_WIDTH"] = int(request.form['window_width'])
    app.config["VERTICAL_STEP"] = int(request.form['vertical_step'])
    app.config["HORIZONTAL_STEP"] = int(request.form['horizontal_step'])
    app.config["HORIZONTAL_SLICE_COUNT"] = int(request.form['horizontal_slice_count'])
    app.config["VETICAL_SLICE_COUNT"] = int(request.form['vertical_slice_count'])



    logging.info('got file: %s', file.filename)
    logging.info('config["TREASHOLD"]: %s', request.form['treashold'])
    logging.info('config["MAX_DETECTIONS"]: %s', request.form['max_count'])
    logging.info('config["IS_DEBUG"]: %s', request.form['is_debug'])
    logging.info('config["SESSION_ID"]: %s', str(request.form['session_id']))
    logging.info('config["IMAGE_HEIGHT"]: %s', str(request.form['image_height']))
    logging.info('config["IMAGE_WIDTH"]: %s', str(request.form['image_width']))
    logging.info('config["WINDOW_HEIGHT"]: %s', str(request.form['window_height']))
    logging.info('config["WINDOW_WIDTH"]: %s', str(request.form['window_width']))
    logging.info('config["VERTICAL_STEP"]: %s', str(request.form['vertical_step']))
    logging.info('config["HORIZONTAL_STEP"]: %s', str(request.form['horizontal_step']))
    logging.info('config["HORIZONTAL_SLICE_COUNT"]: %s', str(request.form['horizontal_slice_count']))
    logging.info('config["VETICAL_SLICE_COUNT"]: %s', str(request.form['vertical_slice_count']))



    file_name = secure_filename(file.filename)
        # Move the file form the temporal folder to
        # the upload folder we setup
    saved_file_name = os.path.join(app.config['UPLOAD_FOLDER'], file_name)
    file.save(saved_file_name)
    logging.info('file savedfile: %s', saved_file_name)
    return detect(file_name, start_time)

def delete_files(folder):
	""""""
	for the_file in os.listdir(folder):
		file_path = os.path.join(folder, the_file)
		if os.path.isfile(file_path):
			os.unlink(file_path)
	return

def pyramid(image,window_size):
	"""Returns yelds of resized images"""
	height, width = image.shape[:2]
	if width/height > 2:
		scale = math.sqrt(image.shape[0]/window_size[1]/1.05)
	else:
		scale = math.sqrt(image.shape[1]/window_size[0]/1.05)
	logging.debug("=======================")
	logging.debug("image.shape[1]:%s, window_size[0]:%s", image.shape[1], window_size[0])
	logging.debug("scale:%s" ,scale)

	file_name = 'id_{1}_origins/origin{0}.jpeg'.format(image.shape[1] , app.config["SESSION_ID"])
	if app.config["IS_DEBUG"] == 'True':
		cv2.imwrite(file_name, image)
	logging.debug("writing image: %s", file_name)
	yield (image,image.shape[1])


	# keep looping over the pyramid
	while True:
		# compute the new dimensions of the image and resize it
		image_width = int(image.shape[1] / scale)
		logging.debug("=======================")
		logging.debug("image.shape[1]:%s, scale:%s", image.shape[1], scale)
		image = imutils.resize(image, width=image_width)
		file_name = 'id_{1}_origins/origin{0}_id{1}.jpeg'.format(image_width , app.config["SESSION_ID"])
		logging.debug("writing image: %s", file_name)
		if app.config["IS_DEBUG"] == 'True':
			cv2.imwrite(file_name, image)

		if image.shape[0] < window_size[1] or image.shape[1] < window_size[0]:
			logging.debug("image size w:%sxh:%s smaller than w:%sXh:%s", image.shape[1], image.shape[0], window_size[0], window_size[1])
			break
		yield (image,image_width)


def sliding_window(image, horizontal_step, vertical_step, window_size):

	"""slide a window across the image"""
	for y in range(0, image.shape[0]-window_size[1], vertical_step):
		for x in range(0, image.shape[1]-window_size[0], horizontal_step):
			logging.debug("yeld image x:%s-%s y:%s-%s]", x, x+window_size[0], y, y+window_size[1])
			yield (x, y, image[y:y + window_size[1], x:x + window_size[0]])

def scan_image(original_file_name, start_time, sess, softmax_tensor, isBatch):
	""" """
	logging.debug("starting scan_image")
	flag_find_id = False
	# load the image and define the window width and height

	image = cv2.imread(original_file_name)
	logging.debug("file was read")
	height, width = image.shape[:2]

	#resize image in case the image is big
	res = image
	(window_width, window_height) = (app.config["WINDOW_WIDTH"] , app.config["WINDOW_HEIGHT"]) #220X120

	if height > width:
		scale = 240/width
		dim = (240,int(height * scale))
		logging.info('portrait')
	else:
		logging.info('landscape')
		scale = 426/width
		dim = (426,int(height * scale))
	logging.debug('dim:%s', dim)
	res = cv2.resize(image, dim,interpolation = cv2.INTER_AREA)
	logging.info('original image resized to %sX%s', image.shape[0], image.shape[1])

	label_lines = [line.rstrip() for line in tf.gfile.GFile('retrained_labels.txt')]

	max_new_id_score = 0
	max_license_score = 0
	max_old_id_score = 0
	new_id_count = 0
	old_id_count = 0
	license_count = 0
	total_count = 0
	# loop over the image pyramid
	for (resized, pyrmid_size) in pyramid(res, window_size=(window_width, window_height)):
		height, width = resized.shape[:2]
		logging.debug("image height:%s,width%s", height, width)
		vertical_step = min(int((height - window_height)/app.config["VETICAL_SLICE_COUNT"]),55) #app.config["VERTICAL_STEP"]
		horizontal_step = min(int((width - window_width)/app.config["HORIZONTAL_SLICE_COUNT"]),100) #app.config["HORIZONTAL_STEP"]
		logging.debug("step calculated h:%s, v:%s", horizontal_step, vertical_step)
		if flag_find_id:
			break
		# loop over the sliding window for each layer of the pyramid
		for (x, y, window) in sliding_window(resized, horizontal_step, vertical_step,\
				window_size=(window_width, window_height)):
			logging.debug('#####################################################')

			if flag_find_id:
				break
			logging.warning('starting image with x:%s,y:%s', x, y)

			# if the window does not meet our desired window size, ignore it
			if window.shape[0] != window_height or window.shape[1] != window_width:
				logging.warning('out of size: %sX%s shoud be at least %sX%s', window.shape[0],\
				window.shape[1], window_height, window_width)
				continue
			total_count += 1
			file_name = ("%s-%s.jpg" %(y, x))
			cv2.imwrite(file_name, window)
			image_data = tf.gfile.FastGFile(file_name, 'rb').read()


			logging.warning('prediction started')
			predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0' : image_data})
			logging.warning('prediction ended')
			top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
			first_label = label_lines[top_k[0]]
			if first_label == "idn":
				new_id_count += 1
			if first_label == "ido":
				old_id_count += 1
			if first_label == "license":
				license_count += 1

			score_string = "%s,%s" %(y, x)
			for node_id in top_k:
				human_string = label_lines[node_id]
				score = predictions[0][node_id]
				logging.warning('%s (score = %.5f)', human_string, score)
				score_string = "%s,%s-%s" %(score_string, human_string, score)
				if human_string == "idn":
					max_new_id_score = max(max_new_id_score, score)
				if human_string == "license":
					max_license_score = max(max_license_score, score)
				if human_string == "ido":
					max_old_id_score = max(max_old_id_score, score)
			file_name = ("results/%s, x-%s,y-%s,score-%s,ssesid-%s.jpg" % (pyrmid_size, x, y, score_string, app.config["SESSION_ID"]))

			if app.config["IS_DEBUG"] == 'True':
				cv2.imwrite(file_name, window)
			if max_new_id_score > app.config["TREASHOLD"] or max_old_id_score > app.config["TREASHOLD"]\
				or max_license_score > app.config["TREASHOLD"]\
				or new_id_count > app.config["MAX_DETECTIONS"]\
				or license_count > app.config["MAX_DETECTIONS"]\
				or old_id_count > app.config["MAX_DETECTIONS"]:

				logging.debug("MAX OR TREASHOLD CONDITION FIRED")
				flag_find_id = True
				break
		if flag_find_id:
			break


	end_time = datetime.now()
	time_taken = (end_time - start_time).seconds
	logging.info('app ended, end time %s', end_time)
	return_data = {"ido": float(max_old_id_score), "old_id_count":old_id_count,\
		"idn": float(max_new_id_score), "new_id_count":new_id_count, \
		"licence": float(max_license_score), "license_count":license_count,		"time taken": time_taken,\
		"total_count":total_count, "file_name":original_file_name}
	if isBatch == False:
		sess.close()
	logging.info('return data:%s', return_data)
	return return_data

def detect(file_name, start_time):
	"""Running detect init """
	file_name = os.path.join(app.config['UPLOAD_FOLDER'], file_name)
	logging.debug("starting graph")
	tf.import_graph_def(app.grafh_def, name='')
	logging.debug("graph file imported")
	with tf.Session() as sess:
		tf.import_graph_def(app.grafh_def, name='')
		logging.debug("graph file imported")
		softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
		logging.debug("starging Session")
		return jsonify(scan_image(file_name, start_time, sess,softmax_tensor, False))

if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True
    )
